<?php
if(isset( $_REQUEST['intent'] ) && $_REQUEST['intent'] == 'labels'){
    $service = "FEDEX_GROUND";
    if ($_POST['selected'] == "second-day") {
        $service = "FEDEX_2_DAY";
    } else if ($_POST['selected'] == "overnight") {
        $service = "STANDARD_OVERNIGHT";
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FAILONERROR => true,
        CURLOPT_URL => 'https://sheikah.amgservers.com/api/label',
    ));
    $key = curl_exec($curl);
    if (!$key)
        die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));

    openssl_public_encrypt("28rxHW2sovSxuExp" . " " . "CKbPQw7mO5aJkwxvLX2Gfl0NL" . " " . "241279715" . " " . "112929182", $crypted, $key);
    curl_close($curl);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://sheikah.amgservers.com/api/label/fedex',
        CURLOPT_FAILONERROR => true,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'auth' => base64_encode($crypted),
            'service' => $service,
            'shipper' => filter_var($_POST['company'], FILTER_SANITIZE_STRING),
            'ship_name' => filter_var($_POST['sender_name'], FILTER_SANITIZE_STRING),
            'ship_addr1' => filter_var($_POST['address'], FILTER_SANITIZE_STRING),
            'ship_city' => filter_var($_POST['city'], FILTER_SANITIZE_STRING),
            'ship_state' => filter_var($_POST['state'], FILTER_SANITIZE_STRING),
            'ship_code' => filter_var($_POST['zip'], FILTER_SANITIZE_STRING),
            'ship_phone' => filter_var($_POST['phone'], FILTER_SANITIZE_STRING),
            'ship_country' => 'US',
            'to_name' => 'YDL Concert',
            'to_addr1' => '3001 Keller Springs Road',
            'to_state' => 'TX',
            'to_city' => 'Carrollton',
            'to_code' => '75006',
            'to_phone' => '7853541981',
            'to_country' => 'US',
        )
    ));
    $resp = curl_exec($curl);
    curl_close($curl);

    $resp = json_decode($resp);
    ?>
    <div id="response">
    <?php
    if (!$resp->packages || sizeof($resp->packages) > 1)
        die("no packages");
    ?>
        <embed src="data:application/pdf;base64,<?php echo $resp->packages[0]->label; ?>" style="width:100%;min-height:400px;" />
    </div>
    <?php    

    die();
}
