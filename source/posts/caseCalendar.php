<?php


    //Check if form has been filled out, if not then display, if yes display calendar
    if(isset($_POST['dateSelected']) && isset($_POST['product'])){
        
        
        $inLabProcessingDays=$_POST['product'];
        $shippingInboundDays = 2;
        $shippingOutboundDays = 2;
        $preProcessingDays = 1;
        
        $convertDate = $_POST['dateSelected'];
        $selectedDate  = date("Y-m-d", strtotime($convertDate));
        $cursorCurrentDay = date('Y-m-d', strtotime($selectedDate));
        $processingArray = [];
        
        $shipOnSaturdays = false;

        $currentYear = date("Y");

        // holidays
        $holidays = [
            "$currentYear-01-01",
            date('Y-m-d', strtotime("third monday of january $currentYear")),
            date('Y-m-d', strtotime("last monday of may $currentYear")),
            "$currentYear-07-04",
            date('Y-m-d', strtotime("first monday of september $currentYear")),
            date('Y-m-d', strtotime("fourth thursday of november $currentYear")),
            "$currentYear-12-24",
            "$currentYear-12-25",
            date('Y-m-d', strtotime("first day of january next year"))
        ];

        // Begin inbound shipping length calculator
        $shippingInboundDaysRemaining = $shippingInboundDays;
        while ($shippingInboundDaysRemaining > 0) {
            $date = date('Y-m-d', strtotime($cursorCurrentDay));
            $day = date('D', strtotime($cursorCurrentDay));
            if (in_array($date, $holidays))
                $processingArray[$date] = "holiday";
            else if ($day == "Sun" || ($day == "Sat" && $shipOnSaturdays))
                $processingArray[$date] = "weekend";
            else {
                $processingArray[$date] = "shipping day";
                $shippingInboundDaysRemaining--;
            }
            $cursorCurrentDay = date('Y-m-d', strtotime($cursorCurrentDay. '+ 1 day'));
        }
        // End inbound shipping length calculator

        // Begin preprocessing length calculator
        $preProcessingDaysRemaining = $preProcessingDays;
        while ($preProcessingDaysRemaining > 0) {
            $date = date('Y-m-d', strtotime($cursorCurrentDay));
            $day = date('D', strtotime($cursorCurrentDay));
            if (in_array($date, $holidays))
                $processingArray[$date] = "holiday";
            else if ($day == "Sun" || $day == "Sat")
                $processingArray[$date] = "weekend";
            else {
                $processingArray[$date] = "pre-processing day";
                $preProcessingDaysRemaining--;
            }
            $cursorCurrentDay = date('Y-m-d', strtotime($cursorCurrentDay. '+ 1 day'));
        }
        // End preprocessing length calculator

        // Begin in-lab processing length calculator
        $inLabProcessingDaysRemaining = $inLabProcessingDays;
        while ($inLabProcessingDaysRemaining > 0) {
            $date = date('Y-m-d', strtotime($cursorCurrentDay));
            $day = date('D', strtotime($cursorCurrentDay));
            if (in_array($date, $holidays))
                $processingArray[$date] = "holiday";
            else if ($day == "Sun" || $day == "Sat")
                $processingArray[$date] = "weekend";
            else {
                $processingArray[$date] = "processing day";
                $inLabProcessingDaysRemaining--;
            }
            $cursorCurrentDay = date('Y-m-d', strtotime($cursorCurrentDay. '+ 1 day'));
        }
        // End in-lab processing length calculator

        // Begin outbound shipping length calculator
        $shippingOutboundDaysRemaining = $shippingOutboundDays;
        while ($shippingOutboundDaysRemaining > 0) {
            $date = date('Y-m-d', strtotime($cursorCurrentDay));
            $day = date('D', strtotime($cursorCurrentDay));
            if (in_array($date, $holidays))
                $processingArray[$date] = "holiday";
            else if ($day == "Sun" || $day == "Sat")
                $processingArray[$date] = "weekend";
            else {
                $processingArray[$date] = "shipping day";
                $shippingOutboundDaysRemaining--;
            }
            $cursorCurrentDay = date('Y-m-d', strtotime($cursorCurrentDay. '+ 1 day'));
        }
        // End outbound shipping length calculator

        $processingArray[date('Y-m-d', strtotime($cursorCurrentDay))] = "delivery day";
        
        ?>
        <div class="case-calendar">
            <?php echo getMonth($processingArray); ?>
        </div>
        <?php
    }else{
        ?>
        <div id="temp"></div>
        <?php  
    }
    function getMonth($processingArray) {
        $calendar = '<div><table class="cal_table">';
        
        $processingDays = sizeof($processingArray); 
        
        $processingIndex = sizeof($processingArray) - $processingDays;
        
        $startProcessingDate = array_keys($processingArray)[$processingIndex];
        
        $processMonth = date('F', strtotime($startProcessingDate));
        
        $calendar .= '<tr><th colspan="7" class="month_bkdg">'. $processMonth . '</th></tr>';
        $calendar .= '<tr class="dayNames"><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>';
        $calendar .= '<tr>';
        
        $startDate = date('Y-m-d', strtotime($startProcessingDate . ' last sunday'));
        if (date('w', strtotime($startProcessingDate)) != '0')
            $startDate = date('Y-m-d', strtotime($startDate . ' last sunday'));
        $deliveryDate = array_keys($processingArray)[$processingDays-1];
        $endDate = date('Y-m-d', strtotime($deliveryDate . ' next saturday'));
        if (date('w', strtotime($deliveryDate)) != '6')
            $endDate = date('Y-m-d', strtotime($endDate . ' next saturday'));
        $cursorDate = $startDate;
        
        $nDays = (new datetime($startDate))->diff(new datetime($endDate))->format("%a");
        
        $numProcessDays = null;
        
        for($d = 0; $d <= $nDays; $d++) {
            $date = date('Y-m-d', strtotime($startDate . '+' . $d . ' days'));
            $day = date('j', strtotime($date));
            $dateVal = isset($processingArray[$date]) ? $processingArray[$date] : null;
            
            if(date('w', strtotime($date)) == 0)
                $calendar .= '</tr><tr>';
            if($dateVal == "shipping day"){
                $calendar .= '<td class="ship_day">' . $day . '</td>';
            }elseif($dateVal == "pre-processing day"){
                $calendar .= '<td class="pre_proc_day">' . $day . '</td>';
            }elseif($dateVal == "processing day"){
                $calendar .= '<td class="proc_day">' . $day . '</td>';
            }elseif($dateVal == "holiday"){
                $calendar .= '<td class="holiday">' . $day . '</td>';
            }elseif($dateVal == "delivery day"){
                $calendar .= '<td class="deliv_day">' . $day . '</td>';
            }else{
                $calendar .= '<td class="norm_day">' . $day . '</td>';
            }
        }
        
        $calendar .= '</tr>';
        $calendar .= '</table></div>';
        
        $tmpArray = array_count_values($processingArray);
        $numProcessDays = $tmpArray["processing day"];
        
        //*** Color Mapping Key ***//
        $calendar .= '</div><div class="cal_key"><table><tr><td class="ship_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><span>= Shipping Day</span></td></tr><tr><td class="pre_proc_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><span>= Processing Day</span></td></tr><tr><td class="proc_day" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>= <u><span>In-Lab:</u> ' . $numProcessDays . '</span></td></tr><tr><td class="holiday">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><span>= Holiday </span></td></tr><tr><td class="deliv_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><span>= Est. Delivery Day </span></tr></td></table><div margin-left:15px;font-size:12px">*Based on 1 Day Delivery<br>** 3 days in lab from digital impression submission for modeless, single unit restoration.</div>';
        
        return $calendar;
    }

?>