<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        <meta name="robots" content="noydir" />
        <meta http-equiv="Cache-Control" CONTENT="no-cache">
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        <link rel="icon" type="image/png" href="/img/8261-Favicon.png">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Tinos:400,700,700i|Nunito:300" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    </head>
    <body class="p-{{ $page->getFilename() }}">
        @yield('body')
        <footer>
            <div class="container">
                <div class="row" style="margin-bottom: 25px;">
                    <div class="col-12">
                        <div class="col-12 title-line">
                            <h2><span class="italic">Serving</span>&nbsp;Dental Practices Since 1964</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <h4>Contact Us</h4>
                        <div class="foot-underline"></div>
                        <address>1018 Airport Road, Suite 110 <br>
                        Hot Springs, AR 71913 <br>
                        Office: 501.760.4085</address>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <h4>About Us</h4>
                        <div class="foot-underline"></div>
                        <ul>
                            <li><a href="/about-us/who-we-are/">Who We Are</a></li>
                            <li><a href="/about-us/digital-dentistry/">Digital Dentistry</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <h4>Send a Case</h4>
                        <div class="foot-underline"></div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <ul>
                                    <li><a href="/send-case/new-doctor/">New Dentist</a></li>
                                    <li><a href="/send-case/digital-case/">Send a Digital Impression</a></li>
                                    <li><a href="/img/JPL-Rx-Form.pdf" target="_blank">Rx Form</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <ul>
                                    <li><a href="/send-case/print-ups-label/">Generate Shipping Label</a></li>
                                    <li><a href="/send-case/local-pickup/">Request Local Pick-Up</a></li>
                                    <li><a href="/send-case/request-supplies/">Request Supplies</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="d-flex" style="justify-content: center;">
                            <div style="align-self: center;margin: 0px 10px;"><img src="/img/Made-In-U.S.A-PNG-Image.png" alt="Made in USA"></div>
                            <div style="align-self: center;margin: 0px 10px;"><img src="/img/cdllogo-black.png" alt="CDL Logo"></div>
                            <div style="align-self: center;margin: 0px 10px;"><img src="/img/NADL-Logo.png" alt="NADL Logo"></div>
                        </div>
                    </div>
                </div>
                <div class="row col-12">
                    <div class="foot-border"></div>
                </div>
            </div>
        </footer>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex" style="align-self: center;"><span>Copyright &copy; {{ date("Y") }} - <em>All Rights Reserved</em></span></div>
                    <div class="col-12 col-md-6" style="text-align: right;"><a href="https://amgci.com/?cref=1"><img src="/img/DesignedBy-AMGLogo.svg" alt="Website Design by AMG Creative" width="192" height="45" /></a></div>
                </div>
            </div>
        </div>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <script src="/assets/js/script.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            AOS.init();
        </script>
        @yield('scripts')
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134092016-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-134092016-1');
        </script>

        
        <!-- Browser Compatibility -->
        <script> 
            var $buoop = {required:{e:-0.01,i:999,f:-1,o:0,s:0,c:-1},insecure:true,unsupported:true,api:2018.09,reminder:0,reminderClosed:1,no_permanent_hide:true }; 
            function $buo_f(){ 
             var e = document.createElement("script"); 
             e.src = "//browser-update.org/update.min.js"; 
             document.body.appendChild(e);
            };
            try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
            catch(e){window.attachEvent("onload", $buo_f)}
        </script>
        
    </body>
</html>
