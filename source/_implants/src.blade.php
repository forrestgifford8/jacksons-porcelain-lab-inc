@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Screw-Retained Crown',
    'meta_description' => 'JPL provides screw-retained crowns that utilizes a zirconia or abutment interface.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.implants-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Screw-Retained Crown</h1>
                    <p>Jackson Porcelain provides screw-retained crowns that utilizes a titanium base or abutment interface. Screw-retained crowns eliminate the main cause of implant failure, which is excess wet cement left below the margin. This restoration completely eliminates cement from the operatory, which increases the success rate and safety that accompanies implant surgery. Screw-retained crowns also make retrievability easy, safe, and noninvasive. The screw access hole is filled with composite and opaquer to provide lifelike esthetics. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Screw-Retained-Crown.png" alt="Screw-Retained Crown">
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection