@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'TruAbutment Custom Abutments',
    'meta_description' => 'TruAbutment custom abutments are expertly fabricated to provide individual anatomic contours, emergence profile, and margin placement. '
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.implants-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>TruAbutment Custom Abutments</h1>
                    <p>Jackson Porcelain is proud to provide TruAbutment custom abutments. These custom abutments are expertly fabricated to provide individual anatomic contours, emergence profile, and margin placement. They serve as a superior base for a long-term restoration in comparison to stock alternatives. There are numerous benefits to using TruAbutment custom abutments, including their patient-specific CAD/CAM fabrication, adjustable cement line, and their precise fit. TruAbutment Custom Abutments are available as screw-retained or cemented according to your preferences. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/TruAbutment-Custom-abutment.png" alt="Tru Custom Abutment">
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection