@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Custom OEM Abutment',
    'meta_description' => 'Our custom OEM abutments are compatible with a wide variety of major implant manufacturers, including Nobel Biocare®, Astra, Zimmer Biomet, and more.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.implants-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Custom OEM Abutment</h1>
                    <p>Our custom OEM abutments are compatible with a wide variety of major implant manufacturers, including Nobel Biocare®, Astra, Zimmer Biomet, and more. These abutments are expertly designed and provide ideal anatomical emergence profiles and support of soft tissue and can be seated using either traditional cementation or screw retained. These OEM abutments will also not void the original manufacturer's warranty.</p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Custom-OEM-Abutment.png" alt="Custom OEM Abutment">
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection