@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Angulated Screw Channel',
    'meta_description' => 'JPL provides Angulated Screw Channel single-units and bridges from Nobel Biocare®.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.implants-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Angulated Screw Channel</h1>
                    <p>JPL provides Angulated Screw Channel single-units and bridges from Nobel Biocare®. ASC restorations provide the same benefits of a traditional screw-retained solution, such as increased implant success and improved retrievability. Through the Angulated Screw Channel concept, the esthetic and occlusal access challenges that can occur with traditional screw-retained restorations are solved. The screw access hole can be placed anywhere between 0° to 25° in a 360° radius, which allows for the restoration to be used for nearly any situation.  </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/ASC.png" alt="ASC">
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection