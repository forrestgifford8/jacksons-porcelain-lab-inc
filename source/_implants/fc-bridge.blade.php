@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full-Contour iZir® Bridge',
    'meta_description' => 'The full-contour iZir® Bridge is available as an Angulated Screw Channel restoration and as an all-on-four to six restoration.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.implants-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Full-Contour iZir® Bridge</h1>
                    <p>The full-contour iZir® Bridge is available as a traditional implant retained prosthesis or with an Angulated Screw Channel option. The traditional iZir&reg; is available for 3 unit bridges up to full round houses. Our ASC option is indicated for up to a 5 unit bridge and eliminates the risk of wet cement left below the margin while retrievability is greatly improved. The all-on-four to six restoration utilizes as little abutments as possible to achieve a stable implant-retained solution. The full-contour bridge from iZir® is fabricated out of an incredibly strong zirconia ceramic which has a flexural strength of 1,100 MPa. It utilizes precision titanium implant components. The esthetics of iZir® are also evident as it is multi-shaded and is resistant to chipping.</p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/full-contour-iZir-Bridge.png" alt="iZir ASC Bridge">
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection