@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Implants',
    'meta_description' => 'JPL is your resource for high-quality implant-retained restorations and abutments.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.implants-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Implants</h1>
                    <p>JPL is your resource for high-quality implant-retained restorations and abutments. We restore most implant systems and are proudly partnered with Nobel Biocare®, Astra, Zimmer Biomet, and Tru Abutment. All our abutments are also FDA 510k compliant.  </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
            </div>
        </section>
    </div>
    <section id="prod-listing">
        <div class="container">
            <div class="prod-list-row">
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/implants/custom-abutments">
                            <div class="prod-img">
                                <img src="/img/Custom-OEM-Abutment.png" alt="Custom OEM Abutment">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Custom OEM Abutment</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/implants/truabutment">
                            <div class="prod-img">
                                <img src="/img/TruAbutment-Custom-abutment.png" alt="Tru Custom Abutment">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Tru Custom Abutment</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/implants/src">
                            <div class="prod-img">
                                <img src="/img/Screw-Retained-Crown.png" alt="Screw-Retained Crown">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Screw-Retained Crown</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="prod-list-row" style="justify-content: space-around;">
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/implants/asc">
                            <div class="prod-img">
                                <img src="/img/ASC.png" alt="ASC">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>ASC</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/implants/fc-bridge">
                            <div class="prod-img">
                                <img src="/img/full-contour-iZir-Bridge.png" alt="iZir ASC Bridge">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Full-Contour iZir Bridge</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection