@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Services',
    'meta_description' => 'JPL is your partner in every case, whether you are in need of an expertly shade matched restoration or comprehensive case planning. '
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.services-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Services </h1>
                    <p>Our team is happy to provide comprehensive case planning for even the most complex situation. We want to help you guarantee success, which is why we utilize the latest in digital dentistry and our own expertise to efficiently provide you with the highest quality, patient specific restorations possible.
                    </p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12 col-md-7 d-flex" style="text-align: left;align-self: center;">
                    <div>
                        <h2>Shade Matching</h2>
                        <p>The expert technicians at JPL will efficiently and professional shade match your patient's restoration to ensure a highly esthetic final restoration. We understand that patient satisfaction is your top priority, and you can depend on our friendly team to make your patient comfortable during the appointment. </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/shade-matching.png" alt="Shade Matching Thumbnail">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-7 d-flex" style="text-align: left;align-self: center;">
                    <div>
                        <h2>Case Planning</h2>
                        <p>Our team is happy to provide comprehensive case planning for even the most complex situation. We want to help you guarantee success, which is why we utilize the latest in digital dentistry and our own expertise to efficiently provide you with the highest quality restoration.</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Case-Planning.png" alt="Case Planning Thumbnail">
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection