@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Fixed',
    'meta_description' => 'The fixed restorations from Jackson Porcelain Lab are crafted with extreme precision.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Fixed</h1>
                    <p>The fixed restorations from Jackson Porcelain Lab are crafted with extreme precision. We utilize the latest digital dentistry to ensure every case is fulfilled with the absolute best restoration possible. We under that your patient wants a restoration that not only looks lifelike and natural, but also fits perfectly and has high longevity. We are happy to provide all-ceramic restorations and PMMA Provisionals. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
            </div>
        </section>
    </div>
    <section id="prod-listing">
        <div class="container">
            <div class="prod-list-row">
               <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/zprime">
                            <div class="prod-img">
                                <img src="/img/IPS-ZirCAD-Prime.png" alt="Z-PRIME">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Z-PRIME</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/z-maxx">
                            <div class="prod-img">
                                <img src="/img/Z-maXX-Multi-Layer.png" alt="Z-maXX Multi-Layer Thumb">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Z-maXX Multi-Layer</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/z-plus-fcz">
                            <div class="prod-img">
                                <img src="/img/Z-Plus-FCZ.png" alt="Z-Plus FCZ Thumbnail">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Z-Plus FCZ</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="prod-list-row">
               <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/ips-emax">
                            <div class="prod-img">
                                <img src="/img/IPSemax.png" alt="IPS e.max® Thumbnail">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>IPS e.max®</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/bruxzir">
                            <div class="prod-img">
                                <img src="/img/Lava-posterior.png" alt="PFM">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Bruxzir&reg;</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/veneers">
                            <div class="prod-img">
                                <img src="/img/Veneer.png" alt="Veneer">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>Veneers</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="prod-list-row" style="justify-content: space-around">
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/pmma">
                            <div class="prod-img">
                                <img src="/img/PMMA-provisional.png" alt="PMMA Provisional">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>PMMA Provisional</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="thumb-wrap">
                    <div class="prod-thumb">
                        <a href="/products/fixed/zirk">
                            <div class="prod-img">
                                <img src="/img/Zirlux-16-Anterior-and-Posterior.png" alt="ZirK">
                                <div class="prod-ovrly"></div>
                            </div>
                            <div class="prod-title">
                                <h4>ZirK</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection