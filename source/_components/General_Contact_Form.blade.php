<div id="formContainer">
    <form action="" id="contactForm" method="post" action="" onsubmit="">
        <input type="text" class="firstName" name="firstNamee" placeholder="First Name*" required>
        <input type="text" class="lastName" name="lastName" placeholder="Last Name*" required>
        <input type="text" class="practiceName" name="practiceName" placeholder="Practice Name*" required>
        <input type="text" class="email" name="email" placeholder="Email*" required>
        <input type="text" class="phone" name="phone" placeholder="Phone Number*" required>
        <input type="text" class="address" name="address" placeholder="Address*" required>
        <div class="row">
            <div class="col-sm-12 col-md-6">           
                <input type="text" class="city" name="city" placeholder="City*" required>
            </div>
            <div class="col-12 col-sm-2">
                <select name="state" class="state" placeholder="State" required>
                    <option value="" disabled selected>State</option>
                    <option value="AK">AK</option>
                    <option value="AR">AR</option>	
                    <option value="AZ">AZ</option>
                    <option value="CA">CA</option>
                    <option value="CO">CO</option>
                    <option value="CT">CT</option>
                    <option value="DC">DC</option>
                    <option value="DE">DE</option>
                    <option value="FL">FL</option>
                    <option value="GA">GA</option>
                    <option value="HI">HI</option>
                    <option value="IA">IA</option>	
                    <option value="ID">ID</option>
                    <option value="IL">IL</option>
                    <option value="IN">IN</option>
                    <option value="KS">KS</option>
                    <option value="KY">KY</option>
                    <option value="LA">LA</option>
                    <option value="MA">MA</option>
                    <option value="MD">MD</option>
                    <option value="ME">ME</option>
                    <option value="MI">MI</option>
                    <option value="MN">MN</option>
                    <option value="MO">MO</option>	
                    <option value="MS">MS</option>
                    <option value="MT">MT</option>
                    <option value="NC">NC</option>	
                    <option value="NE">NE</option>
                    <option value="NH">NH</option>
                    <option value="NJ">NJ</option>
                    <option value="NM">NM</option>			
                    <option value="NV">NV</option>
                    <option value="NY">NY</option>
                    <option value="ND">ND</option>
                    <option value="OH">OH</option>
                    <option value="OK">OK</option>
                    <option value="OR">OR</option>
                    <option value="PA">PA</option>
                    <option value="RI">RI</option>
                    <option value="SC">SC</option>
                    <option value="SD">SD</option>
                    <option value="TN">TN</option>
                    <option value="TX">TX</option>
                    <option value="UT">UT</option>
                    <option value="VT">VT</option>
                    <option value="VA">VA</option>
                    <option value="WA">WA</option>
                    <option value="WI">WI</option>	
                    <option value="WV">WV</option>
                    <option value="WY">WY</option>
                </select>
            </div>
            <div class="col-12 col-sm-10 col-md-4">
                <input type="text" class="zipcode" name="zipcode" placeholder="Zip Code*" required>
            </div>
        </div>
        <textarea type="text" class="message" name="message" placeholder="Message:" required></textarea>
        <div style="clear:both"></div>
        
        <div class="g-recaptcha" data-sitekey="6LeSc2sUAAAAAKhACiAHIV3oulug2csgBEky1ese"></div>
        
        <div class="btn"><input class="submit btn" value="Submit" type="submit" data-url="/posts/mailGun.php"></div>
        <div class="responseMessage"></div>
        
    </form>
</div>
<script type="text/javascript">
    
    var wasSent = false;
    
    jQuery('#contactForm').submit(function(event) {
        
        event.preventDefault();
        
        if(!wasSent) {
            var formID = '#contactForm';
            
            var captchaResponse = grecaptcha.getResponse();
            console.log(captchaResponse);
        
            var firstName = jQuery(formID+' .firstName').val();
            var lastName = jQuery(formID+' .lastName').val();
            var practiceName = jQuery(formID+' .practiceName').val();
            var email = jQuery(formID+' .email').val();
            var phone = jQuery(formID+' .phone').val(); 
            var address = jQuery(formID+' .address').val();
            var city = jQuery(formID+' .city').val();
            var state = jQuery(formID+' .state').val();
            var zipcode = jQuery(formID+' .zipcode').val();
            var message = jQuery(formID+' .message').val();

            var emailMessage = '<table style="max-width:500px;width:100%;border:1px solid #dadada;border-collapse:collapse;"><tr style="border-bottom:1px solid #dadada"><th colspan="2" style="font-size:1.1em;background:#29608F;color:#fff;padding:7px;">Contact Form Submission</th></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">First Name:</td><td style="padding:5px;">'+firstName+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Last Name:</td><td style="padding:5px;">'+lastName+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Practice Name:</td><td style="padding:5px;">'+practiceName+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Email:</td><td style="padding:5px;">'+email+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Phone Number:</td><td style="padding:5px;">'+phone+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Address:</td><td style="padding:5px;">'+address+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">City:</td><td style="padding:5px;">'+city+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">State:</td><td style="padding:5px;">'+state+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Zip Code:</td><td style="padding:5px;">'+zipcode+'</td></tr><tr style="border-bottom:1px solid #dadada"><td style="padding:5px;white-space:nowrap;">Message:</td><td style="padding:5px;">'+message+'</td></tr></table>'

            var subject = 'Contact Form Submission';

            var postURL = jQuery(formID+' .submit').data('url');

            jQuery.ajax({
                url: postURL,
                type: 'POST',
                data: {
                    message : emailMessage,
                    subject : subject,
                    capResponse: captchaResponse
                },
                success: function(data) {
                    jQuery(formID+' .responseMessage').append('Your Message has Been Sent Successfully!');
                    jQuery(formID+' .responseMessage').css("display", "block");
                    jQuery(formID+' .submit').attr("value", "Message Sent!");
                    wasSent = true;
                },
                error: function(data) {
                    console.log("error");
                }
            });
        }
    });
    
</script>