<div class="primary-nav d-block d-lg-none" id="primary-nav" menu-expanded="false">
    <div id="close-mobile">X</div>
    <ul class="list-unstyled">
        <li>
            <a href=""  data-target="#about-us-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">About Us</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="about-us-collapse-m" data-parent="#primary-nav">
                <li><a href="/about-us/who-we-are">Who We Are</a></li>
                <li><a href="/about-us/digital-dentistry">Digital Dentistry</a></li>
                <li><a href="/about-us/news-events">News &amp; Events</a></li>
            </ul>
        </li>
        <li>
            <a href=""  data-target="#products-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Products</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="products-collapse-m" data-parent="#primary-nav">
                <li><a href="/products/fixed">Fixed</a></li>
                <li><a href="/products/implants">Implants</a></li>
                <li><a href="/products/services">Additional Services</a></li>
            </ul>
        </li>
        <li>
            <a href=""  data-target="#send-case-collapse-m" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Send a Case</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="send-case-collapse-m" data-parent="#primary-nav">
                <li><a href="/send-case/new-doctor">New Dentist</a></li>
                <li><a href="/send-case/digital-case">Send a Digital Impression</a></li>
                <li><a href="/img/JPL-Rx-Form.pdf" target="_blank">Rx Form</a></li>
                <li><a href="/send-case/print-ups-label">Generate Shipping Label</a></li>
                <li><a href="/send-case/upload-file">Upload a File</a></li>
                <li><a href="https://jpl.labstar.com/site_inc/account_login.asp?dest=%2Findex%2Easp&bRedirect=1" target="_blank">Doctor Login</a></li>
                <li><a href="/send-case/local-pickup">Request Local Pick-Up</a></li>
                <li><a href="/send-case/request-supplies">Request Supplies</a></li>
                <li><a href="/send-case/case-schedular">Case Scheduler</a></li> 
            </ul>
        </li>
        <li><a href="/resources"><span class="h-effect"></span><span class="nav-link-txt">Resources</span></a></li>
        <li><a href="/contact-us"><span class="h-effect"></span><span class="nav-link-txt">Contact Us</span></a></li>
    </ul>
</div>