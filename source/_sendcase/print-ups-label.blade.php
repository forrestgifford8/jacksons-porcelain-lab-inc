@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Generate Shipping Label',
    'meta_description' => 'Print your free shipping label by filling out all necessary information below.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Generate Shipping Label</h1>
                    <p>Please provide all necessary information to generate a complimentary UPS shipping label, which will ensure your case gets to our lab quickly and securely. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-container" style="margin: auto;">
                        <form id="shipping-label-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-labelform" placeholder="Doctor's Name *" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-labelform" placeholder="Practice Name *" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-labelform" placeholder="Phone Number *" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="email-labelform" placeholder="Email Address *" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-labelform" placeholder="Address *" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-labelform" placeholder="City *" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <select class="form-control" id="state-labelform" placeholder="State *" required="required">
                                        <option value="">-- Select State -- *</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-labelform" placeholder="Zipcode *" required="required" type="text" maxlength="5" />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="service" value="03">
                        <input type="hidden" id="public_id" value="1ebdc839bf5b4fde9ac54bda56a06682" />
                        <button type="submit" class="btn-blue">Create Label</button>
                    </form>
                        <div class="loader">Loading...</div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#shipping-label-form ~ .loader').hide();
        $('#shipping-label-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#shipping-label-form ~ .loader').show();
            $('p.form-error').remove();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/label/create',
                data: {
                    id: $('#public_id').eq(0).val(),
                    company: $('#practice-name-labelform').eq(0).val(),
                    name: $('#doctor-name-labelform').eq(0).val(),
                    address: $('#address-labelform').val(),
                    city: $('#city-labelform').eq(0).val(),
                    state: $('#state-labelform').eq(0).val(),
                    zip: $('#zip-labelform').eq(0).val(),
                    phone: $('#phone-labelform').eq(0).val(),
                    service: $('input[name="service"]').eq(0).val()
                },
                success: function(data) {
                    $('#shipping-label-form ~ .loader').hide();
                    $('#shipping-label-form').after('<div style="display: flex; justify-content: center;"><a href="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" target="_blank" class="btn btn-primary" download="UPS-Shipping-Label.pdf">Download Shipping Label</a></div><embed src="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" style="width:100%;min-height:400px;" />');
                    $('#btn-delivery-next').show();
                }, 
                error: function(data) {
                    var error = "An unknown error occured";
                    if (data.responseJSON.data.errors) {
                        error = data.responseJSON.data.errors[0].description;
                    }
                    $('#shipping-label-form ~ .loader').hide();
                    $('#shipping-label-form').show();
                    $('#shipping-label-form button[type="submit"]').before('<p class="form-error" style="color: #b90f0f;">'+error+'</p>');
                }
            });
        });
    });
</script>
@endsection