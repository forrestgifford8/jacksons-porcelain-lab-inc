@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Request Local Pick-Up',
    'meta_description' => 'Our delivery drivers pickup and delivery cases from practices within our local area.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Request Local Pick-Up</h1>
                    <p>JPL provides local pick-up and delivery to all our local clinicians. If you are located in our area, please fill our a request form and one of our delivery drivers will arrive at your practice right away. </p>
                </div>
            </div>
            <div class="row">
                <div class="form-container" style="margin: auto;">
                    <form id="local-pickup-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-label-group mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="ready" id="ready-today" value="Today" />
                                <label class="form-check-label" for="ready-today">
                                    Ready today
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                                <label class="form-check-label" for="ready-tomorrow">
                                    Ready tomorrow
                                </label>
                            </div>
                        </div>
                        <input type="hidden" id="public_id" value="1ebdc839bf5b4fde9ac54bda56a06682" />
                        <div class="g-recaptcha" data-sitekey="6Lc2Ao4UAAAAAL34s9oCQbgPsPAOJ_vHXDqNyUCL"></div>
                        <button type="submit" class="btn-blue">Request Pickup</button>
                    </form>
                    <div class="loader">Loading...</div>
                </div>  
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
        <script type="text/javascript">
    $(document).ready(function() {
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/1ebdc839bf5b4fde9ac54bda56a06682/14b8518822bb4379b59177f1ec5a6e4a',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    ready: $('input[name="ready"]').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request! We\'ll process your pickup shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection