@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Upload a File',
    'meta_description' => 'Our straightforward file uploader allows you to send important case information to our team quickly and efficiently'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Upload a File</h1>
                    <p>Our straightforward file uploader allows you to send important case information to our team quickly and efficiently. Simply drag &amp; drop or search for files, included case photos, DICOM files, shade match photos, or any other documents relevant to your case through the feature below. </p>
                </div>
            </div>
            <div class="row" style="text-align: left;">
                <div class="col-12">
                    <div id="file-upload"><p style="color: red"><small>The file uploader application could not load.<br />Please make sure you're using the latest version of a supported web browser (Chrome, Firefox, Edge, Safari), with javascript enabled.</small></p></div><script type="text/javascript">var loader=function(){var e=document.createElement("script"),t=document.getElementsByTagName("script")[0];e.src="https://goron.amgservers.com/embed/B838B428/uploader.js?v=1.0.4",t.parentNode.insertBefore(e,t)};window.addEventListener?window.addEventListener("load",loader,!1):window.attachEvent("onload",loader);</script>
                </div> 
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection