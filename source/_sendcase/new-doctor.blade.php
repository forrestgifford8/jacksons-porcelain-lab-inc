@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'New Dentist',
    'meta_description' => 'Getting your traditional or digital case on its way to our lab is quick and easy, because every resource you may need is compiled in one convenient location.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>New Dentist</h1>
                    <p>JPL would like to thank you for choosing our team as your dental partner. We want your interactions with us to be hassle-free and easy for every case, which is why we have streamlined our case submission process. Getting your traditional or digital case on its way only takes a few minutes, because every resource you may need is compiled in one convenient location. </p>
                </div>
            </div>
        </section>
        <section id="new-doc-steps">
            <div class="container">
                <div class="row">
                    <div id="step-nums" class="col-sm-12 col-md-10 d-flex">
                        <div id="step1-num" class="step-num active">
                            <img src="/img/8261-1-teal-Icon.png" alt="Step Number">
                            <img src="/img/8261-1-blue-Icon.png" alt="Step Number">
                         </div>
                         <div id="step2-num" class="step-num">
                            <img src="/img/8261-2-teal-Icon.png" alt="Step Number">
                            <img src="/img/8261-2-blue-Icon.png" alt="Step Number">   
                         </div>
                         <div id="step3-num" class="step-num">
                            <img src="/img/8261-3-teal-Icon.png" alt="Step Number">
                            <img src="/img/8261-3-blue-Icon.png" alt="Step Number">  
                         </div>
                         <div id="step4-num" class="step-num">
                            <img src="/img/8261-4-teal-Icon.png" alt="Step Number">
                            <img src="/img/8261-4-blue-Icon.png" alt="Step Number">   
                         </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="step-wraps">
            <div class="container">
                <div class="row">
                    <div id="step1-wrap" class="step-wrap col-sm-12 active">
                        <h2>Choose your Preferred Type of Case</h2>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h3>Send a Traditional Case</h3>
                                <p>With our step-by-step process, you can get your traditional cases to our laboratory right away. Rx forms, shipping labels, local pickup request, and a case scheduler are all located within the following steps.</p>
                                <p class="pt-2"><span id="moveTo-2" class="btn-blue">Get Started</span></p>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <h3>Send a Digital Case</h3>
                                <p>JPL is proud to be a part of the digital dentistry innovation taking place in the industry today. We accept scans from most major intraoral scanners.</p>
                                <p class="pt-2"><a href="/send-case/digital-case/" class="btn-blue">Get Started</a></p>
                            </div>
                        </div>
                    </div>
                    <div id="step2-wrap" class="step-wrap col-sm-12">
                        <h2>Download Rx Form</h2>
                        <p>We provide a printable Rx form for all your cases. Simply download the form and include a completed version with your traditional impression.</p>
                        <p class="pt-2"><a href="/img/JPL-Rx-Form.pdf" class="btn-blue" target="_blank">Download</a></p>
                        <p class="pt-2"><span id="moveTo-3" class="btn-blue">Continue</span></p>
                    </div>
                    <div id="step3-wrap" class="step-wrap col-12">
                        <h2>Choose Your Delivery Method</h2>
                        <p>We want to ensure your case gets to our lab quickly and securely, without causing unnecessary stress to you or your team. We provide free local pickup and delivery, as well as free shipping labels to any clinicians who fall outside our local zone. Provide us with your Zip Code to see if you qualify. </p>
                        <div id="formContainer" class="zipField">
                            <form method="post" name="Form" id="FORM" action="">
                                <input name="zipcode" type="var" placeholder="Enter Zip Code Here" id="zipcode_Field"><br>
                                <input class="btn-blue mt-3" value="Submit" type="submit" style="width: 250px">
                            </form>
                            <div class="placeholder"></div>   
                        </div>
                        <div class="form-container" style="margin: auto;">
                            <form id="shipping-label-form" class="d-none" action="">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="doctor-name-labelform" placeholder="Doctor's Name *" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="practice-name-labelform" placeholder="Practice Name *" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="phone-labelform" placeholder="Phone Number *" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="email-labelform" placeholder="Email Address *" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="address-labelform" placeholder="Address *" required="required" type="text" />
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-label-group mb-3" style="margin: auto;">
                                            <input class="form-control" id="city-labelform" placeholder="City *" required="required" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-lg-2 px-lg-0">
                                        <div class="form-label-group mb-3" style="margin: auto;">
                                            <select class="form-control" id="state-labelform" placeholder="State *" required="required">
                                                <option value="">-- Select State -- *</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District of Columbia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-label-group mb-3" style="margin: auto;">
                                            <input class="form-control" id="zip-labelform" placeholder="Zipcode *" required="required" type="text" maxlength="5" />
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="service" value="03">
                                <input type="hidden" id="public_id" value="1ebdc839bf5b4fde9ac54bda56a06682" />
                                <button type="submit" class="btn-blue btn-primary">Create Label</button>
                            </form>
                            <form id="local-pickup-form" class="d-none" action="">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-label-group mb-3" style="margin: auto;">
                                            <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-lg-2 px-lg-0">
                                        <div class="form-label-group mb-3" style="margin: auto;">
                                            <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-label-group mb-3" style="margin: auto;">
                                            <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-label-group mb-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ready" id="ready-today" value="Today" />
                                        <label class="form-check-label" for="ready-today">
                                            Ready today
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                                        <label class="form-check-label" for="ready-tomorrow">
                                            Ready tomorrow
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" id="public_id" value="1ebdc839bf5b4fde9ac54bda56a06682" />
                                <div class="g-recaptcha" data-sitekey="6Lc2Ao4UAAAAAL34s9oCQbgPsPAOJ_vHXDqNyUCL"></div>
                                <button type="submit" class="btn-blue btn-primary">Request Pickup</button>
                            </form>
                            <div class="loader">Loading...</div>
                        </div>
                        <p class="pt-2"><span id="moveTo-4" class="btn-blue d-none">Continue</span></p>
                    </div>
                    <div id="step4-wrap" class="step-wrap col-sm-12">
                        <h2>You're Finished!</h2>
                        <p>We hope you found this case submission process easy and stress-free. The next time you send your case to JPL, we invite you to pick and choose the resources you need from the Send a Case menu above. There you can print Rx forms, shipping labels, and schedule local pickup anytime you need. </p>
                        <h3>Optional: Schedule Your Case</h3>
                        <p>Our team prides ourselves on punctual cases. We know punctuality is a key factor in patient satisfaction and that a late case can greatly interfere with your practice's workflow. We offer a convenient case scheduling calendar, which will calculate when your case will be returned to your practice. Simply provide us with your case information and we will let you know when you can expect your restoration to arrive. </p>
                        <a href="/send-case/case-schedular/" class="btn-blue">Schedule Your Case</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#moveTo-2').click(function(){
            $('#step1-wrap').removeClass('active');
            $('#step2-wrap').addClass('active');
            $('#step1-num').removeClass('active');
            $('#step2-num').addClass('active');
        });
        $('#moveTo-3').click(function(){
            $('#step2-wrap').removeClass('active');
            $('#step3-wrap').addClass('active');
            $('#step2-num').removeClass('active');
            $('#step3-num').addClass('active');
        });
        $('#moveTo-4').click(function(){
            $('#step3-wrap').removeClass('active');
            $('#step4-wrap').addClass('active');
            $('#step3-num').removeClass('active');
            $('#step4-num').addClass('active');
        });
        
        //UPS Label
        $('#shipping-label-form ~ .loader').hide();
        $('#shipping-label-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#shipping-label-form ~ .loader').show();
            $('p.form-error').remove();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/label/create',
                data: {
                    id: $('#public_id').eq(0).val(),
                    company: $('#practice-name-labelform').eq(0).val(),
                    name: $('#doctor-name-labelform').eq(0).val(),
                    address: $('#address-labelform').val(),
                    city: $('#city-labelform').eq(0).val(),
                    state: $('#state-labelform').eq(0).val(),
                    zip: $('#zip-labelform').eq(0).val(),
                    phone: $('#phone-labelform').eq(0).val(),
                    service: $('input[name="service"]').eq(0).val()
                },
                success: function(data) {
                    $('#shipping-label-form ~ .loader').hide();
                    $('#shipping-label-form').after('<div style="display: flex; justify-content: center;"><a href="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" target="_blank" class="btn-blue btn-primary" download="UPS-Shipping-Label.pdf">Download Shipping Label</a></div><embed src="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" style="width:100%;min-height:400px;" />');
                    $('#btn-delivery-next').show();
                }, 
                error: function(data) {
                    var error = "An unknown error occured";
                    if (data.responseJSON.data.errors) {
                        error = data.responseJSON.data.errors[0].description;
                    }
                    $('#shipping-label-form ~ .loader').hide();
                    $('#shipping-label-form').show();
                    $('#shipping-label-form button[type="submit"]').before('<p class="form-error" style="color: #b90f0f;">'+error+'</p>');
                }
            });
        });
        
        //Local Pickup
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/1ebdc839bf5b4fde9ac54bda56a06682/b467fc3f8f254983a91d7d0107f3ef2f',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
        
        //Zip Code Checker
        var zipCodes= ['71913','71910','71914','71902','71903','71901','71956','71964','72104','71968','71929','71909','72087','71949','71933','71941','71942','71921','72167','71999','71998','72019','72122','71920','72015','72128','72158','72018','71970','72126','71923'];


        function validateForm_Zip(zipcode) {
            if (zipcode==null || zipcode=="" || zipcode.length != 5) {
                window.alert("Please Enter a Valid Zipcode");
                return false;
            } else {
                return true;
            }
        }
        function isInArray(value, array) {
            return array.includes(value);
        }
        $('#FORM').submit(function(e){
            var zipcode=document.forms["Form"]["zipcode"].value;
            e.preventDefault();

            if( validateForm_Zip(zipcode) == false ){
                return;
            }
            if( isInArray(zipcode, zipCodes) == false ){
                $('.placeholder').empty()
                $('.placeholder').append('<div class="row"><div class="col-sm-12"><p style="margin: 15px 0px;">Your practice does not qualify for local pickup and delivery. Instead, you can print a complimentary shipping label, so your case can get to our lab right away.</div></div>')
                $('#local-pickup-form').hide();
                $('#moveTo-4').removeClass('d-none');
                $('#shipping-label-form').removeClass('d-none');
            } else {
                $('.placeholder').empty()
                $('.placeholder').append('<div class="row"><div class="col-sm-12"><p style="margin: 15px 0px;">Hello, Neighbor! Your practice qualifies for free pickup and delivery. We invite you to schedule a local pickup by providing us with your practice\'s location. If you would prefer a complimentary shipping label, this resource can be found beneath Send a Case in the menu above. </div></div>')
                $('#local-pickup-form').removeClass('d-none');
                $('#moveTo-4').removeClass('d-none');
                $('#local-pickup-form').removeClass('d-none');
            } 
        });
    });
</script>
@endsection