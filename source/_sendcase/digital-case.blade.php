@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Send a Digital Impression',
    'meta_description' => 'Jackson Porcelain accepts files from most major intraoral scanners. Please find your preferred scanner in the list below and follow the protocols to get your case to our lab.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Send a Digital Impression</h1>
                    <p>JPL accepts files from most major intraoral scanners. Current customers please use the LabStar portal for your digital impression file submissions. For new customers please use the manufacturer's protocols listed below for your convenience. Current customers please our online portal for file submission</p>
                    <p><a href="https://jpl.labstar.com/site_inc/account_login.asp?dest=%2Findex%2Easp&bRedirect=1" class="btn-blue" target="_blank">Labstar Login</a></p>
                </div>
            </div>
            <div class="row" style="text-align: left;">
                <div class="col-12">
                    <div id="accordion">
                    <h3>iTero®</h3>
                    <div>
                        <ul>            
                            <li>Call 800-577-8767</li>
                            <li>Select Option 1</li>
                            <li>Request that JPL is added to your scanner and Identify our lab using our phone number: 501.760.4085 or our Itero # 88704</li>
                            <li>After JPL has been added, restart your scanner</li>
                            <li>After connecting to us as a lab, select JPL on your scanner when sending files</li>
                        </ul>
                    </div>
                    <h3>Trios®</h3> 
                    <div>
                        <ul>            
                            <li>Go to us.3shapecommunicate.com in a web browser</li>
                            <li>Connect with JPL as Lab by searching jacksonslab3shape@gmail.com   </li>
                            <li>After connecting to us as a Lab, select JPL when sending files</li>
                        </ul>
                    </div>
                    <h3>3M™ True Definition Scanner</h3>
                    <div>
                        <ul>            
                            <li>Call 3M™ support at 800-634-2249 select Option 3</li>
                            <li>Select Option 1 and request support JPL be added to your scanner</li>
                            <li>3M™ will then confirm with JPL and add connection remotely</li>
                        </ul>
                    </div>
                    <h3>E4D-Planmeca® Emerald</h3>
                    <div> 
                        <ul>            
                            <li>Select Find a lab option on your scanner</li>
                            <li>Search for either JPL or jacksonslab3shape@gmail.com  </li>
                            <li>Add JPL</li>
                            <li>Select JPL when submitting scans</li>
                        </ul>
                    </div>
                    <h3>Carestream®</h3>
                    <div>
                        <b>Option: 1</b>
                        <ul>                        
                            <li>Email JPL at jacksonslab3shape@gmail.com   </li>
                        </ul>
                        <b>Option: 2</b>
                        <ul>                        
                            <li>Visit Carestream Connect on your scanner</li>
                            <li>Search for JPL</li>
                            <li>Add JPL</li>
                            <li>Select JPL when submitting scans raw STL files</li>
                            <li>Email JPL at jacksonslab3shape@gmail.com  </li>
                        </ul>
                    </div>
                    <h3>Medit i500/i700</h3>
                    <div>
                        <ol>
                            <li>Sign up for your Medit Link Account by visiting <a href="https://www.meditlink.com/register" target="_blank">MeditLink's registration page</a>.</li>
                            <li>Request JPL as a partner and we will accept the request.</li>
                            <li>Send your cases directly through Medit Link.</li>
                        </ol>
                    </div>
                    <h3>Sirona CEREC</h3>
                    <div>
                        <ul>
                            <li>After reviewing the scan you will then select “export to STL file,” from the drop down menu </li>
                            <li>Once you have exported and saved the STL file, login into the <strong><u><a href="https://jpl.labstar.com/site_inc/account_login.asp?dest=%2Findex%2Easp&bRedirect=1" target="_blank">Labstar Portal</a></u></strong> to complete your file submission to Jackson Porcelain </li>
                            <li>First time customer will need to call the lab to set up Labstar account </li>

                        </ul>
                    </div>
                </div>
                </div> 
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection