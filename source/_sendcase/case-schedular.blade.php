@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Case Scheduler',
    'meta_description' => 'We are happy to provide an easy-to-use case scheduling calendar which will give you the date when you can expect your case to be returned.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Case Scheduler</h1>
                    <p>JPL is happy to provide an easy-to-use case scheduling calendar which will give you the date when you can expect your case to be returned. </p>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                         <div class="form-container">
                            <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                            <form id="caseCal-Form" method="post" name="CalForm" action="">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="date-calendarform" placeholder="Select a Ship Date" required="required" type="text" />
                                </div>
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <select class="form-control" name="product" id="product-calendarform" placeholder="Select a Product">
                                        <option value="" style="background-color: #f6f6f6;" disabled selected>Select a Product</option>
                                        <option value="6">Trios/Intra Oral Scans</option>
                                        <option value="8">e.max</option>
                                        <option value="8">Bruxzir</option>
                                        <option value="8">Z-Plus</option>
                                        <option value="8">Z-maXX</option>
                                        <option value="8">PFZ</option>
                                        <option value="11">PFM</option>
                                        <option value="8">Full Cast (Gold or White)</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn-blue">Get Turnaround Time</button>
                            </form>
                            <div class="loader">Loading...</div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#caseCal-Form').submit(function(e) {
            e.preventDefault();
            $('#caseCal-Form ~ .loader').show();
            $('.case-calendar').remove();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/case-calendar',
                data: {
                    date: $('#date-calendarform').eq(0).val(),
                    product: $('#product-calendarform').eq(0).val(),
                    shiptime: "1",
                    processingtime: "0",
                    holidays: '["2018-11-21", "2018-11-23", "2018-12-26", "2018-12-31"]'
                },
                success: function(data) {
                    $('#caseCal-Form ~ .loader').hide();
                    $('#caseCal-Form').parent('.form-container').after(atob(data.calendar));
                    $('#delivery-disclaimer').show();
                }, 
                error: function() {

                }
            });
        });

        $( "#date-calendarform" ).datepicker();
    });
</script>
@endsection