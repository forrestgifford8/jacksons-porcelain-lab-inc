@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Request Supplies',
    'meta_description' => 'JPL gladly provides you with any necessary supplies for your case. Please submit a request below for case boxed, Rx forms, or our fee schedule.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.sendcase-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Request Supplies</h1>
                    <p>JPL gladly provides you with any necessary supplies for your case. Please submit a request below for case boxed, Rx forms, or our fee schedule.</p>
                </div>
            </div>
            <div class="row">
                <div class="form-container" style="margin: auto;">
                    <form id="request-supplies-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-suppliesform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-suppliesform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-suppliesform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-suppliesform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-suppliesform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-suppliesform" placeholder="State" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-suppliesform" placeholder="Zipcode" required="required" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <textarea id="message-suppliesform" class="form-control" placeholder="What supplies do you need?"></textarea>
                        </div>
                        <input type="hidden" id="public_id" value="1ebdc839bf5b4fde9ac54bda56a06682" />
                        <div class="g-recaptcha" data-sitekey="6Lc2Ao4UAAAAAL34s9oCQbgPsPAOJ_vHXDqNyUCL"></div>
                        <button type="submit" class="btn-blue">Request Supplies</button>
                    </form>
                    <div class="loader">Loading...</div>
                </div>  
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#request-supplies-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#request-supplies-form .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/1ebdc839bf5b4fde9ac54bda56a06682/4949503bf1484dd58de6b9d2d2e6dc42',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-suppliesform').eq(0).val(),
                    name: $('#doctor-name-suppliesform').eq(0).val(),
                    phone: $('#phone-suppliesform').eq(0).val(),
                    address: $('#address-suppliesform').eq(0).val(),
                    city: $('#city-suppliesform').eq(0).val(),
                    state: $('#state-suppliesform').eq(0).val(),
                    zip: $('#zip-suppliesform').eq(0).val(),
                    supplies: $('#message-suppliesform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#request-supplies-form').after('<p>Thanks for your request! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#request-supplies-form').show();
                    formPending = false;
                    $('#request-supplies-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection