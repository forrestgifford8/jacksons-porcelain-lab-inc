@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Contact Us',
    'meta_description' => 'If you have any comments or concerns, please contact JPL through phone or email.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.contact-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Contact Us </h1>
                    <p>The team at JPL Laboratory greatly appreciates any feedback we receive. If you have any comments or concerns, please contact us through one of the means listed below. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4" style="text-align: left;">
                    <p>Jacksons Porcelain Laboratory <br> 
                    1018 Airport Road, Suite 110 <br>
                    Hot Springs, AR 71913</p>
                    <p>Office: 501-760-4085 </p>
                </div>
                <div class="col-sm-12 col-md-8">
                    <form id="contact-form" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <textarea id="message-contactform" class="form-control" placeholder="What can we do for you?"></textarea>
                        </div>
                        <input type="hidden" id="public_id" value="1ebdc839bf5b4fde9ac54bda56a06682" />
                        <div class="g-recaptcha" data-sitekey="6Lc2Ao4UAAAAAL34s9oCQbgPsPAOJ_vHXDqNyUCL"></div>
                        <button class="btn-blue btn-primary mt-3" type="submit">Send Now</button>
                    </form>
                    <div class="loader">Loading...</div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/1ebdc839bf5b4fde9ac54bda56a06682/975b0977dd52452b8a9350953761e09e',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection