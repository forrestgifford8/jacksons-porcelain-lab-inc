@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'TRIOS® Loaner Program',
    'meta_description' => 'JPL is proud to support digital dentistry through our TRIOS® Loaner Program. Discover all the features and benefits of the TRIOS® scanner today!'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.trios-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>TRIOS® Loaner Program</h1>
                    <p>JPL is committed to the integration of digital dentistry into the restorative workflow, because we have witnessed first-hand the benefits. We are proud to support digital dentistry through our TRIOS® Loaner Program. Discover all the features and benefits of the TRIOS® scanner today! We invite you to schedule an appointment with our team to get started with our TRIOS® Loaner Program. This program makes it easy to integrate digital dentistry into your practice by allowing you to try before you buy or providing a scanner on demand.</p>
                    <p><a href="/img/TRIOS-loaner-agreement.pdf" class="btn-blue" target="_blank">TRIOS&reg; Loaner Agreement</a></p>
                    <p><a href="/contact-us" class="btn-blue">Schedule Your Appointment</a></p>
                    <h2>3Shape TRIOS® Scanner</h2>
                    <p>This digital dentistry innovation provides digital impressions quickly and easily. Through the use of HD photos and lifelike color to enhance detail, each impression ensure a high level of accuracy for the case. There are multiple scanner options to choose from, TRIOS® can meet various economic and laboratory requirements. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h3>Benefits</h3>
                    <p>• Available in a wireless edition, which allows for non-stop <br> scanning due to simple batter exchange and long-lasting rechargeable batteries. • <br>
                    • Provides optimal comfort for patients • <br>
                    • Fast and easy scanning ensures reduces chair time • <br>
                    • High accuracy on single units, quads, and full arch • <br>
                    • Improved patient dialogue and treatment evaluations • <br>
                    • Shade measurement with a higher level of reliability than the human eye • <br>
                    • HD photos • <br>
                    • Intraoral camera • <br>
                    • Instant online communication with technicians for consultation • <br></p>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection