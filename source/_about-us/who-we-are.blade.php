@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Who We Are',
    'meta_description' => 'JPL was founded in 1964 by Stoney Jackson. Today, a 2nd generation of owners have combined Stoney\'s vision for the lab with a dedication to digital dentistry.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.about-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Who We Are </h1>
                    <p>JPL was founded in 1964, by Stoney Jackson with the goal to do things better and smarter with a focus on quality and service. In 1987, Karen and Nick Perry acquired the laboratory.</p>

                    <p>Karen and Nick remain committed to the laboratory's dedication for quality and service. They are also excitedly looking towards the future for the business, which revolves around a 3rd generation of owner, their son Brandon. Since 2010, the lab has operated with a focus on integrating digital dentistry into the workflow.</p> 
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <h2>The Laboratory You Can Choose with Confidence</h2>
                    <p>• Certified Dental Laboratory since 1997 •<br>
                    • Beta Lab for Nobel Biocare® •<br>
                    • Beta Lab for LabStar Software •<br>
                    • Committed to Digital Integration •<br> 
                    • Made in America Restorations •<br>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection