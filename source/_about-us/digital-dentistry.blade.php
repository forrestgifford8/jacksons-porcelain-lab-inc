@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Digital Dentistry',
    'meta_description' => 'JPL is proud to utilize the latest technology in all our cases to ensure every patient receives the benefits of the latest innovations.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.digital-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Digital Dentistry</h1>
                    <p>JPL is a dedicated digital dentistry resource for clinicians throughout the nation. We are proud to utilize the latest technology in all our cases to ensure every patient receives the benefits of the latest innovations. We invite you to discover all the benefits that come with partnering with Jacksons Porcelain Laboratory.  </p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <h2>Digital Dentistry Benefits</h2>
                    <p>• Consistent and High-Quality Restorations • <br>
                    • Reduced Chair Time • <br>
                    • Streamlined Digital Workflow • <br>
                    • Hassle-Free Digital Submission • <br>
                    • Direct Communication with Staff Throughout Case •</p>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection