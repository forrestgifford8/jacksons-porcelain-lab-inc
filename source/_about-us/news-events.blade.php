@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'News &amp; Events',
    'meta_description' => 'JPL are proud of our continued innovation, and invite you to check back often to discover the latest news and events coming out of our laboratory.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.about-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>News &amp; Events</h1>
                    <p>JPL is dedicated to maintaining its position on the cutting-edge of digital dentistry. We are proud of our continued innovation, and invite you to check back often to discover the latest news and events coming out of our laboratory. </p> 
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection