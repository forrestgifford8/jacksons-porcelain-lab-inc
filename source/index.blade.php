@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'The Premier Arkansas-Based CDL',
    'meta_description' => 'JPL Laboratory, Inc. is a fixed and implant-focused Certified Dental Laboratory based in Hot Springs, Arkansas.'
    ])
@endsection

@section('body')
@include('_partials.home-header')
<main role="main" class="home-page">
    <section>
        <div class="container">
           <div class="row">
               <div class="col-12 title-line">
                    <h2><span class="italic">Expertly-Crafted</span>&nbsp;Restorations and Services</h2>
                </div>
           </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 home-products-cta">
                    <div class="cta-wrap-l">
                        <h3>Fixed</h3>
                        <p>Our crown and bridge restorations are fabricated with high precision. Your patients will be highly satisfied with the lifelike esthetics and extreme durability, which are possible through our use of the best materials possible and digital dentistry.</p>
                        <p><a href="/products/fixed/" class="btn">Learn More</a></p>
                    </div>
                </div>
                <div class="col-12 col-md-7" data-aos="fade-left" data-aos-offset="300" data-aos-duration="500">
                    <img src="/img/8261-Fixed-HomePg-Rectangle.png" alt="Fixed Home Thumbnail">
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
               <div class="col-12 col-md-7" data-aos="fade-right" data-aos-offset="250" data-aos-duration="500">
                    <img src="/img/8261-Implants-HomePg-Rectangle.png" alt="Implants Home Thumbnail">
                </div>
                <div class="col-12 col-md-5 home-products-cta">
                    <div class="cta-wrap-r">
                        <h3>Implants</h3>
                        <p>JPL is proud to provide OEM and TruAbutment custom abutments that are FDA 510k compliant. In addition, we offer innovative solutions, such as the Angulated Screw Channel, screw-retained crowns, and iZir implant-retained crown and bridges.</p>
                        <p><a href="/products/implants/" class="btn">Learn More</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 home-products-cta">
                    <div class="cta-wrap-l">
                        <h3>Services</h3>
                        <p>The team at JPL is your partner in every case. We provide value-added services to help you increase patient satisfaction and streamline the restorative process. We invite you to contact us to schedule an appointment for shade matching or case planning.</p>
                        <p><a href="/products/services/" class="btn">Learn More</a></p>
                    </div>
                </div>
                <div class="col-12 col-md-7" data-aos="fade-left" data-aos-offset="300" data-aos-duration="500">
                    <img src="/img/8261-Services-HomePg-Rectangle.png" alt="Services Home Thumbnail">
                </div>
            </div>
        </div>
    </section>
    <section id="home-digital-cta">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12" style="border: 0px !important;" data-aos="fade-down" data-aos-offset="300" data-aos-duration="500">
                    <img src="/img/8261-Digital-Icon.png" alt="Digital Icon">
                    <h3>Discover JPL's Digital Capabilities</h3>
                    <p><a href="/about-us/digital-dentistry/" class="btn-blue">Learn More</a></p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
           <div class="row">
               <div class="col-12 title-line">
                    <h2>Prescribe&nbsp;<span class="italic">Quality</span>&nbsp;Restorations Today!</h2>
                </div>
           </div>
        </div>
    </section>
    <section style="margin-bottom: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" style="text-align: center;color: #fff;">
                    <p>JPL's online case submission process eliminates the hassle that typically accompanies traditional cases. Everything you need to get your case to our team is located on our website, including Rx forms, shipping labels, local pick-up request forms, and more. We also provide all our digital protocols, so you can be confident in sending either a traditional or digital case to our lab. </p>
                    <p><a href="/send-case/new-doctor/" class="btn">Learn More</a></p>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection