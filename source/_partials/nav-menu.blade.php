<nav class="primary-nav d-none d-lg-block" id="primary-nav" menu-expanded="false" style="text-align: right;">
    <a href="https://jpl.labstar.com/site_inc/account_login.asp?dest=%2Findex%2Easp&bRedirect=1" class="btn" target="_blank">Doctor Login</a>
    <ul class="list-unstyled d-lg-flex">
        <li>
            <a href=""  data-target="#about-us-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">About Us</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="about-us-collapse" data-parent="#primary-nav">
                <li><a href="/about-us/who-we-are">Who We Are</a></li>
                <li><a href="/about-us/digital-dentistry">Digital Dentistry</a></li>
                <li><a href="/about-us/news-events">News &amp; Events</a></li>
            </ul>
        </li>
        <li>
            <a href=""  data-target="#products-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Products</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="products-collapse" data-parent="#primary-nav">
                <li><a href="/products/fixed">Fixed</a></li>
                <li><a href="/products/implants">Implants</a></li>
                <li><a href="/products/services">Additional Services</a></li>
            </ul>
        </li>
        <li>
            <a href=""  data-target="#send-case-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Send a Case</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="send-case-collapse" data-parent="#primary-nav">
                <li><a href="/send-case/new-doctor">New Dentist</a></li>
                <li><a href="/send-case/digital-case">Send a Digital Impression</a></li>
                <li><a href="/img/JPL-Rx-Form.pdf" target="_blank">Rx Form</a></li>
                <li><a href="/send-case/print-ups-label">Generate Shipping Label</a></li>
                <li><a href="/send-case/upload-file">Upload a File</a></li>
                <li><a href="https://jpl.labstar.com/site_inc/account_login.asp?dest=%2Findex%2Easp&bRedirect=1" target="_blank">Doctor Login</a></li>
                <li><a href="/send-case/local-pickup">Request Local Pick-Up</a></li>
                <li><a href="/send-case/request-supplies">Request Supplies</a></li>
                <li><a href="/send-case/case-schedular">Case Scheduler</a></li> 
            </ul>
        </li>
        <li><a href="/resources"><span class="h-effect"></span><span class="nav-link-txt">Resources</span></a></li>
        <li><a href="/contact-us"><span class="h-effect"></span><span class="nav-link-txt">Contact Us</span></a></li>
    </ul>
</nav>
<div id="mobile-nav-icon" class="d-block d-lg-none">
    <div>
        <svg id="hamIcon" viewBox="0 0 32 32"><path class="st0" style="fill:#fff" d="M29.4 10H2.6c-.6 0-1-.4-1-1s.4-1 1-1h26.8c.6 0 1 .4 1 1s-.5 1-1 1zM29.4 17H2.6c-.6 0-1-.4-1-1s.4-1 1-1h26.8c.6 0 1 .4 1 1s-.5 1-1 1zM29.4 24H2.6c-.6 0-1-.4-1-1s.4-1 1-1h26.8c.6 0 1 .4 1 1s-.5 1-1 1z"></path></svg>
    </div>
</div>