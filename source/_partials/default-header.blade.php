<header>
    <div class="container">
        <div class="row">
            <div class="d-flex col-12" id="main-header">
                <div>
                    <a href="/">
                        <img id="header-logo" src="/img/Jacksons_Porcelain_Lab_Logo.png" alt="Jackson's Dental Lab, Inc. Logo">
                    </a>
                </div>
                <div>
                    <div class="main-nav-wrap">
                        @include('_partials.nav-menu')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mobile-menu" class="d-sm-block d-lg-none">
        @include('_components.nav-mobile')
    </div>
</header>