<header id="home-banner">
    <div class="container">
        <div class="row">
            <div class="d-flex col-12" id="main-header">
                <div>
                    <a href="/">
                        <img id="header-logo" src="/img/Jacksons_Porcelain_Lab_Logo.png" alt="Jackson's Dental Lab, Inc. Logo">
                    </a>
                </div>
                <div>
                    <div class="main-nav-wrap">
                        @include('_partials.nav-menu')
                    </div>
                </div>
            </div>
        </div>
        <div id="home-banner-txt"> 
            <div class="row">
                <div class="col-12" data-aos="fade-down" data-aos-duration="600" data-aos-delay="100">
                    <h1>Your Resource for <span class="italic">Personalized</span> Service and <span class="italic">High-Quality</span> Restorations</h1>
                </div>
            </div>
            <div id="home-banner-btns" class="row" data-aos="fade-up" data-aos-duration="600" data-aos-delay="100">
                <div class="col-sm-12 col-md-6">
                    <a href="/send-case/new-doctor/" class="btn">Get Started</a>
                </div>
                <div class="col-sm-12 col-md-6">
                    <a href="/contact-us/" class="btn">Contact Us</a>
                </div>
            </div>
        </div>
        <section id="home-intro-txt">
            <div class="row">
                <div class="col-sm-12 col-md-7" data-aos="fade-right" data-aos-duration="500" style="padding-right: 0px;">
                    <div class="home-txt-wrap">
                        <h2> The Premier Arkansas-Based, Family-Owned CDL Since 1964</h2>
                        <p>Jacksons Porcelain Laboratory, Inc. is a fixed and implant-focused Certified Dental Laboratory based in Hot Springs, Arkansas. Backed by years of experience, our team of technicians fabricate exceptional crown and bridge restorations, as well as high-quality implant-based products and abutments in our state-of-the-art laboratory. Our friendly staff also provides value-added services for every case, such as custom shade matching and implant planning with surgical guides. JPL is proud to be nationally certified by the NBC since 1997. We have also served as beta labs for both Nobel Biocare® and LabStar Software</p>
                        <a href="/about-us/who-we-are/" class="btn-blue">Learn More</a>
                    </div>
                </div>
                <div id="intro-txt-img" class="d-none d-md-block col-md-5" data-aos="fade-left" data-aos-duration="500"></div>
            </div>
        </section>
    </div>
    <div id="mobile-menu" class="d-sm-block d-lg-none">
        @include('_components.nav-mobile')
    </div>
</header>