@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'ZirK',
    'meta_description' => 'Give every patient an expertly crafted crown no matter their budget. ZirK from JPL is our economy priced full-contour zirconia crown and bridge solution.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>ZirK</h1>
                    <p>Give every patient an expertly crafted crown no matter their budget. ZirK from JPL is our economy priced full-contour zirconia crown and bridge solution. Every ZirK restoration is crafted in-house at our Arkansas laboratory, so you can prescribe with confidence. This option takes 10 days in-lab and never compromises on quality or services. It is available as a solid full-contour monochromatic or multilayer option. ZirK is only available in a single shade and comes with a 3-year warranty. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Zirlux-16-Anterior-and-Posterior.png" alt="ZirK">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                        <h3>Indications</h3>
                        <div>
                            <p>Full-contour zirconia is so versatile, it can be used in almost any situation from singles, bridges with any combination of abutments and pontics, inlay bridges and screw-retained implants. Also an esthetic alternative to a PFM with metal occlusion due to limited space.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <p>When esthetic expectations are high and it is important that the restorations match surrounding natural dentition or other existing restorations. If bonding is necessary to retain the restoration, bond strength is weaker and less predictable than other ceramics.</p>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>Shoulder preparation not needed. A mild chamfer or a feather-edge margin is good. 1mm buccal, lingual, and occlusal reduction is ideal, but can go to .5mm in some areas when reduction is limited. Minimum occlusal reduction of 0.5 mm; 1 mm is ideal. Adjustments and polishing: Adjust full-contour zirconia crowns and bridges using water and air spray to keep the restoration cool and to avoid microfractures with a fine grit diamond. If using air only, use the lightest touch possible when making adjustments. A football-shaped bur is the most effective for occlusal and lingual surfaces (on anterior teeth); a tapered bur is the ideal choice for buccal and lingual surfaces. Polish full-contour zirconia restorations with the porcelain polishing system of your choice.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <p>It is recommended that full-contour zirconia be cemented using a zirconia primer like Z-Prime from Bisco or Clearfil Ceramic Primer from Kuraray. Alternatively, a resin reinforced glass ionomer such as RelyX Luting cement can also be used. When a greater bond is needed due to the lack of a retentive preparation, use a resin cement like RelyX Unicam or RelyX Ultimate. Before cementing all full-contour zirconia crowns, the interior surface of the crown needs to be cleaned with Ivoclean (Ivoclar Vivadent - Amherst, NY). This is critical in assuring maximum bond strength.</p>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>Solid zirconia requires a cast gold type preparation. If adjustments are needed, use zirconia specific diamonds and rubber wheels polishing with diamond paste.</p>
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown – Porcelain/Ceramic Substrate</li>
                                <li>D6245 Pontic Porcelain/Ceramic</li>
                                <li>D6740 Abutment Crown Porcelain/Ceramic</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection