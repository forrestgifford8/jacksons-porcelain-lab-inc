@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Veneers',
    'meta_description' => 'The veneers from JPL are fabricated out of IPS e.max®. These veneers benefit from CAD/Press technology, which ensures expert design and esthetics.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Veneers</h1>
                    <p>Veneers offer a less intrusive solution than crowns and braces. They are attached to the surface of a patient's preexisting smile and follow the contours of their teeth to provide an esthetic cosmetic solution in the cases of chipped, discolored, or gapped teeth. The veneers from JPL are fabricated out of IPS e.max®. These veneers benefit from CAD/Press technology, which ensures expert design and lifelike esthetics. IPS e.max® has a flexural strength of 500 MPa for a long-lasting cosmetic solution.</p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Veneer.png" alt="Veneer">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Changed color of anterior teeth, Incorrect shape of tooth or position in dental arch, Enamel defects such as enamel hypoplasia, attrition of teeth as a consequence of trauma, wide interproximal spaces like diastema</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Bruxism and parafunction, Pathology of bite, more than 50% of enamel affected by pathology</p>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                       <p>For facial reduction three wheel diamond depth cutter should be used for orientation grooves. For proximal reduction - round end tapered diamond bur is used as an extension for facial reduction.</p>
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection