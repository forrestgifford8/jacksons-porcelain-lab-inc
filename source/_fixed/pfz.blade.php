@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'PFZ',
    'meta_description' => 'PFZ benefits from the inherent translucent esthetics of zirconia, which serves as the substructure for the porcelain overlay.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>PFZ</h1>
                    <p>Porcelain-fused-to-zirconia restorations are an excellent option given their superior esthetics when compared to porcelain-fused-to-metal crowns. PFZ benefits from the inherent translucent esthetics of zirconia, which serves as the substructure for the porcelain overlay. Instead of having to deal with unsightly discoloration or black along the gum line, PFZs will disappear alongside your patient's natural teeth for a beautiful and unbroken smile. Our expert technicians utilize the latest CAD/CAM equipment to ensure every PFZ restoration has perfect fit and esthetics.</p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/PFZ.png" alt="PFZ Thumb">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                    <h3>Indications</h3>
                        <div>
                            <p>A CAD/CAM substitute for traditional PFM, our porcelain-fused-to-zirconia can be used for anterior and posterior crowns, crowns over implants, and bridges of up to fourteen units.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <ul>
                                <li>Attachment cases</li>
                                <li>Cases with less than 1 mm clearance</li>
                                <li>Bruxism</li>
                                <li>Patients who have broken a PFM crown</li>
                                <li>Cases that require bonding</li>
                            </ul>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>The ideal preparation for PFZs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <ul>
                                <li>Resin Ionomer cement (RelyX or RelyX Unicem, 3M ESPE)</li>
                                <li>Maxcem Elite (Kerr)</li>
                                <li>Panavia F 2.0 (Kuraray) - ideal for short, tapered preparations</li>
                                <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            </ul>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Brasseler, Shofu, Vident).</p>
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown - porcelain / ceramic substrate</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection