@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full Cast',
    'meta_description' => 'Full cast are fabricated with  CAD/CAM technology to ensure precision, consistent fit, and finish when compared with their traditionally created alternatives.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Full Cast</h1>
                    <p>The full cast crowns from JPL are available in gold or white gold metal alloys. These restorations are fabricated with state-of-the-art CAD/CAM technology to ensure superior precision, consistent fit, and finish when compared with their traditionally created alternatives. These full cast crowns are durable and gentle on opposing dentition. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Full-Cast-Crown.png" alt="Full Cast Thumbnail">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Full-cast gold crowns are indicated for crowns, veneers, inlays, onlays, and bridges.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Full-cast gold crowns are contraindicated for partials and implants.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Inlays and onlays can also be fabricated as a full-cast restoration. Feather-edge margin preparations are indicated for full-cast restorations, but any margin preparation may be used.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <ul>
                            <li>Panavia 21 (Must be tin plated if precious metal is used)</li>
                            <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            <li>Zinc Phosphate Polycarboxylate Resin Ionomer cement (RelyX, 3M ESPE)</li>
                        </ul>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <p>All castings are made with a metal alloy, be it non-precious, semi-precious or precious metals. Alloys are classified by their content.</p>
                                <ul>
                                    <li>Base – contents include non-precious, Chrome Cobalt or Titanium</li>
                                    <li>Noble – consists of 25 percent precious alloy</li>
                                    <li>High Noble – consists of 60 percent precious metal with at least 40 percent being gold</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6">
                                <p>Alloy type refers to the hardness and/or softness of the material.</p>
                                <ul>
                                    <li>Type I – Extra soft</li>
                                    <li>Type II – Soft</li>
                                    <li>Type III– Hard</li>
                                    <li>Type IV – Extra Hard (Rigid)</li>
                                    <li>Non-Precious, Noble 20, White High Noble – Type IV – Very hard and rigid. These crowns are more difficult to adjust and re-polish than alloys with a high gold content.</li>
                                    <li>Full Cast 40 – Type III – Yellow high noble alloy. Brand name currently used is Argenco 40 HN.</li>
                                    <li>Full Cast 52 HN – Type III – Yellow high noble alloy. Brand name currently used is Argenco 52.</li>
                                    <li>Full Cast 75- Type III – Yellow high noble and is an upgrade from full cast 52. The gold is slightly more yellow in color. Brand name currently used Argenco 75.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2790 Crown Full-Cast High Noble Metal</li>
                            <li>D2791 Crown Full-Cast Predominantly Base Metal</li>
                            <li>D2792 Crown Full-Cast Noble Metal</li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection