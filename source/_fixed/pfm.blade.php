@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'PFM',
    'meta_description' => 'Porcelain-fused-to-metal, a traditional solution, is crafted by fusing a layer of esthetic porcelain to a substructure of high-quality metal alloy.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>PFM</h1>
                    <p>Due to their long history of clinical success, porcelain-fused-to-metal restorations are considered a timeless standard for dental restorations. This traditional solution is crafted by fusing a layer of esthetic porcelain to a substructure of high-quality metal alloy. The expert technicians at JPL carefully bond the porcelain to the metal to ensure no discoloration and an optimal esthetic finish. These restorations are biocompatible and help ensure the continued periodontal health of your patient. PFMs are gentle on opposing teeth. We utilize CAD/CAM technology on this metal-based solution, so that each case is fulfilled with high precision. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/PFM_3-Unit.png" alt="PFM">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                        <h3>Indications</h3>
                        <div>
                            <p>Our PFMs can be used for crowns and bridges (up to fourteen units). PFMs can be manufactured to nonprecious, semiprecious, and yellow high noble copings and can be used in conjunction with cast partials and implants.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <p>Contraindicated when the patient has a metal allergy or when the size of the tooth pulp is negligibly smaller, thus compromising the tooth preparation process. It is also contraindicated when the clinical tooth crown is very short and lacks the required stability including retention that is enough to provide the space for porcelain and metal.</p>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>The ideal preparation for PFMs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <ul>
                                <li>Panavia 21 – tin plated</li>
                                <li>Glass ionomer cement (GC Fuji, GC America)</li>
                                <li>Zinc Phosphate Polycarboxylate</li>
                                <li>Resin Ionomer cement (RelyX, 3M ESPE)</li>
                            </ul>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Brasseler, Shofu, Vident).</p>
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2750 Crown Porcelain fused to high noble</li>
                                <li>D2751 Crown Porcelain fused to nonprecious</li>
                                <li>D2752 Crown Porcelain fused to semiprecious</li>
                                <li>D6750 Crown Porcelain fused to high noble (bridge units)</li>
                                <li>D6751 Crown Porcelain fused to nonprecious (bridge units)</li>
                                <li>D6752 Crown Porcelain fused to semiprecious (bridge units)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection