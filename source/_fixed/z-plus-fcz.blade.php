@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Z-Plus FCZ',
    'meta_description' => 'Z-Plus Full-Contour Zirconia offers the high strengths inherent with zirconia restorations along with lifelike esthetics.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Z-Plus FCZ</h1>
                    <p>Z-Plus Full-Contour Zirconia offers the high strengths inherent with zirconia restorations along with lifelike esthetics. Available exclusively from JPL, Z-Plus FCZ is the ideal solution for posterior or anterior cases that require a long-lasting restoration that will seamlessly blend in with surrounding dentition. Fabricated out of high-quality zirconia material, Z-Plus is crafted by our expert technicians in our Arkansas laboratory. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Z-Plus-FCZ.png" alt="Z-Plus FCZ">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                        <h3>Indications</h3>
                        <div>
                            <p>Fully anatomical crowns and 3-unit to multi-unit bridges in the anterior and posterior, anatomically reduced crowns in the anterior or posterior, 3-unit to multi-unit bridge frameworks, multi-unit and screw-retained restorations on Ti bases.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <p>When esthetic expectations are high and it is important that the restorations match surrounding natural dentition or other existing restorations. If bonding is necessary to retain the restoration, bond strength is weaker and less predictable than other ceramics.</p>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>Shoulder preparation not needed. A mild champfer or a feather edge margin is good. 1mm buccal, lingual and occlusal reduction is ideal, but can go to .5mm in some areas, when reduction is limited.
Minimum occlusal reduction of 0.5 mm; 1 mm is ideal.
Adjustments and polishing: Adjust Full-contour crowns and bridges using water and air spray to keep the restoration cool and to avoid micro-fractures with a fine grit diamond. If using air only, use the lightest touch possible when making adjustments. A football-shaped bur is the most effective for occlusal and lingual surfaces (on anterior teeth); a tapered bur is the ideal choice for buccal and lingual surfaces.
Polish Full-contour zirconia restorations with the porcelain polishing system of your choice.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <p>May be cemented using a resin reinforced glass ionomer such as Relyx Luting cement. When a greater bond is needed do to the lack of a retentive preparation, use resin cement like Relyx Unicam or Relyx Ultimate.
Before cementing all Full-contour zirconia crowns, the interior surface of the crown needs to be cleaned with Ivoclean (Ivoclar Vivadent; Amherst N.Y.). This is critical in assuring maximum bond strength. </p>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>Z-Plus FCZ has a flexural strength of 1,000 +/- 150 Mpa, which makes it an excellent choice for the posterior. Zirconia requires a cast gold type preparation. If adjustments are needed, use zirconia specific diamonds and rubber wheels polishing with diamond paste.</p> 
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown – Porcelain/Ceramic Substrate</li>
                                <li>D6245 Pontic Porcelain/Ceramic</li>
                                <li>D6740 Abutment Crown Porcelain/Ceramic</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection