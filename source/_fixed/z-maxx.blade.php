@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Z-maXX Multi-Layer',
    'meta_description' => 'JPL was founded in 1964 by Stoney Jackson. Today, a 2nd generation of owners have combined Stoney\'s vision for the lab with a dedication to digital dentistry.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Z-maXX Multi-Layer</h1>
                    <p>Z-maXX Multi-Layer from JPL is a high-quality full-contour zirconia option. In addition to its high-strength, which is incomparable to alternative all-ceramic materials, this full-contour zirconia offer multi-layered, ultra-translucent esthetics. Due to its multiple layers, this zirconia retains its esthetics even after any last minute adjustments. It also offers smooth shade transitions and natural color progression. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Z-maXX-Multi-Layer.png" alt="Z-maXX Multi-Layer">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                        <h3>Indications</h3>
                        <div>
                            <p>Veneers, inlays, onlays, fully anatomical crowns and bridges (maximum 3 units extending to the molar region). </p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <p>Long-span bridges and frameworks (4+ units), and anatomically reduced crown and bridge frameworks.</p>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>Supragingival preparation allowed. Ceramic-appropriate preparation (i.e. avoid sharp corners, edges, and internal angles. Circumferential shoulder with rounded internal angle. Structure-conserving preparation ideal (required minimum thickness of 0.5mm). Preparation margin should be clearly visible. Prepared tooth height: 3mm minimum.   </p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <p>May be cemented using a resin reinforced glass ionomer such as Relyx Luting cement. When a greater bond is needed do to the lack of a retentive preparation, use resin cement like Relyx Unicam or Relyx Ultimate.
Before cementing all full-contour zirconia crowns, the interior surface of the crown needs to be cleaned with Ivoclean (Ivoclar Vivadent; Amherst N.Y.). This is critical in assuring maximum bond strength. </p>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>If adjustments are needed, use zirconia specific diamonds and rubber wheels polishing with diamond paste. </p> 
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown – Porcelain/Ceramic Substrate</li>
                                <li>D6245 Pontic Porcelain/Ceramic</li>
                                <li>D6740 Abutment Crown Porcelain/Ceramic</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection