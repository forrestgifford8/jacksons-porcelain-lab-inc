@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Z-Plus Hybrid',
    'meta_description' => 'Our zirconia hybrid restoration is fabricated out of our exceptional Z-Plus and utilizes esthetic porcelain. It is available as a micro-cutback with porcelain layering.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.fixed-img-header')
        <section class="intro-txt-prod">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Z-Plus Hybrid</h1>
                    <p>Our zirconia hybrid restoration is fabricated out of our exceptional Z-Plus and utilizes esthetic porcelain. Available as a micro-cutback with porcelain layering on the facial, this restorative option is ideal for anterior cases. The Z-Plus zirconia ensures strength for a long-lasting solution. The porcelain provides translucent beauty that will allow the crown to seamlessly blend in with the rest of your patient's smile. </p>
                    <p><a href="/send-case/new-doctor" class="btn-blue">Get Started</a></p>
                </div>
                <div class="col-sm-12 col-md-5">
                    <img src="/img/Anterior-Crown.png" alt="Z-Plus Hybrid Thumbnail">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="accordion">
                    <h3>Indications</h3>
                        <div>
                            <p>A CAD/CAM substitute for traditional PFM, our porcelain-fused-to-zirconia can be used for anterior and posterior crowns, crowns over implants, and bridges of up to fourteen units.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <ul>
                                <li>Attachment cases</li>
                                <li>Cases with less than 1 mm clearance</li>
                                <li>Bruxism</li>
                                <li>Patients who have broken a PFM crown</li>
                                <li>Cases that require bonding</li>
                            </ul>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>The ideal preparation for PFZs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <ul>
                                <li>Resin Ionomer cement (RelyX or RelyX Unicem, 3M ESPE)</li>
                                <li>Maxcem Elite (Kerr)</li>
                                <li>Panavia F 2.0 (Kuraray) - ideal for short, tapered preparations</li>
                                <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            </ul>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Brasseler, Shofu, Vident).</p>
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown - porcelain / ceramic substrate</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection