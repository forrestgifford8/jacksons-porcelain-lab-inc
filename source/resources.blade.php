@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Resources',
    'meta_description' => 'JPL is happy to support your practice in various ways, including providing valuable resources that can be downloaded and printed at your convenience. '
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.resources-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Resources</h1>
                    <p>JPL is happy to support your practice in various ways, including providing valuable resources that can be downloaded and printed at your convenience. </p>
                </div>
            </div>
            <div class="row" style="margin-top: 2rem;">
                 <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="resource-btn">
                        <a href="/img/JPL-Rx-Form.pdf" target="_blank">
                            <div class="res-bkgd-wrap"><div class="res-bkgd res-rx"></div></div>
                            <h4>Rx Form</h4>
                            <hr>
                            <div class="btn-blue">Download</div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="resource-btn">
                        <a href="/img/Torque-Spec-Sheet.pdf" target="_blank">
                            <div class="res-bkgd-wrap"><div class="res-bkgd res-specs"></div></div>
                            <h4>Implant Torque Specs</h4>
                            <hr>
                            <div class="btn-blue">Download</div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="resource-btn">
                        <a href="/img/8261-JPL-Tru-Abutment-Torque-Spec-Sheet.pdf" target="_blank">
                            <div class="res-bkgd-wrap"><div class="res-bkgd res-truAbut"></div></div>
                            <h4>TruAbutment Torque Specs</h4>
                            <hr>
                            <div class="btn-blue">Download</div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="resource-btn">
                        <a href="/img/Cementation-Guide.pdf" target="_blank">
                            <div class="res-bkgd-wrap"><div class="res-bkgd res-cement"></div></div>
                            <h4>Cementation Guide</h4>
                            <hr>
                            <div class="btn-blue">Download</div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="resource-btn">
                        <a href="/img/ADA-Code-List.pdf" target="_blank">
                            <div class="res-bkgd-wrap"><div class="res-bkgd res-code"></div></div>
                            <h4>ADA Code List</h4>
                            <hr>
                            <div class="btn-blue">Download</div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="resource-btn">
                        <a href="/img/DI-Comparison-Chart.pdf" target="_blank">
                            <div class="res-bkgd-wrap"><div class="res-bkgd res-di"></div></div>
                            <h4>DI Chart</h4>
                            <hr>
                            <div class="btn-blue">Download</div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection