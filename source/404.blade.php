@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Error 404',
    'meta_description' => 'Page Not Found.'
    ])
@endsection

@section('body')
@include('_partials.default-header')
<main role="main">
    <div id="page-wrap" class="container">
        @include('_partials.contact-img-header')
        <section class="intro-txt">
            <div class="row">
                <div class="col-12">
                    <h1>Error 404 - Page Not Found </h1>
                    <p><a href="/#" class="btn-blue">Home</a></p>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection